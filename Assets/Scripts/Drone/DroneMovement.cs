﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneMovement : MonoBehaviour
{
    public FloatVariable droneSpeed;
    public float range;
    public float attackRate;
    public LayerMask enemyLayer;
    public GameObject bullet;
    
    private Vector2 _moveTarget;
    private Transform _enemyTarget;
    private float _nextAttack;
 
    void Start () {
        _moveTarget = transform.position;
    }
     
    void Update () {
        if (Input.GetMouseButtonDown(0)) {
            _moveTarget = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(_moveTarget, Vector2.zero, 0, enemyLayer);
            if (hit)
            {
                _enemyTarget = hit.transform;
            }
            else
            {
                _enemyTarget = null;
            }
        }
        
        if (_enemyTarget != null)
        {
            if (Vector2.Distance(transform.position, _enemyTarget.position) > range)
            {
                transform.position =
                    Vector2.MoveTowards(transform.position, _enemyTarget.position, droneSpeed.value * Time.deltaTime);
            }
            else if (Time.time > _nextAttack)
            {
                Attack();
                _nextAttack = Time.time + attackRate;
            }
        }
        else
        {
            transform.position =
                Vector2.MoveTowards(transform.position, _moveTarget, droneSpeed.value * Time.deltaTime);
        }
    }

    private void Attack()
    {
        Instantiate(bullet, transform.position, transform.rotation).GetComponent<DroneBullet>().target = _enemyTarget;
    }
}
