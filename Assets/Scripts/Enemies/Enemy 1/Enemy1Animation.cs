<<<<<<< HEAD
﻿using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

public class Enemy1Animation : MonoBehaviour
{
    public AIPath aiPath;
    public Animator animator;
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
    }

    
    void Update()
    {
        if (GetComponent<Enemy1Movement>().movementStopped == false)
        {
            if (aiPath.velocity.x >= 1)
            {
                animator.SetFloat("Horizontal",1f); 
            }
            else if (aiPath.velocity.x <= -1)
            {
                animator.SetFloat("Horizontal",-1f);
            }
            else
            {
                animator.SetFloat("Horizontal",0f);
            }

            if (aiPath.velocity.y >= 1)
            {
                animator.SetFloat("Vertical",1f);
            }
            else if (aiPath.velocity.y <= -1)
            {
                animator.SetFloat("Vertical", -1f);
            } 
            else
            {
                animator.SetFloat("Vertical",0f);
            }
        }

        if (aiPath.velocity.x == 0 && aiPath.velocity.y == 0)
        {
            GetComponent<Enemy1Movement>().movementStopped = true;
            GetComponent<Enemy1Attack>().isAttacking = true;

        }
        else
        {
            GetComponent<Enemy1Movement>().movementStopped = false;
            GetComponent<Enemy1Attack>().isAttacking = false;
        }
        
    }
}
=======
﻿using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UnityEngine;

public class Enemy1Animation : MonoBehaviour
{
    public AIPath aiPath;
    public Animator animator;
    void Start()
    {
        animator = GetComponentInChildren<Animator>();
    }

    
    void Update()
    {
        if (GetComponent<Enemy1Movement>().movementStopped == false)
        {
            if (aiPath.velocity.x >= 1)
            {
                animator.SetFloat("Horizontal",1f); 
            }
            else if (aiPath.velocity.x <= -1)
            {
                animator.SetFloat("Horizontal",-1f);
            }
            else
            {
                animator.SetFloat("Horizontal",0f);
            }

            if (aiPath.velocity.y >= 1)
            {
                animator.SetFloat("Vertical",1f);
            }
            else if (aiPath.velocity.y <= -1)
            {
                animator.SetFloat("Vertical", -1f);
            } 
            else
            {
                animator.SetFloat("Vertical",0f);
            }
        }

        if (aiPath.velocity.x == 0 && aiPath.velocity.y == 0)
        {
            GetComponent<Enemy1Movement>().movementStopped = true;
            GetComponent<Enemy1Attack>().isAttacking = true;

        }
        else
        {
            GetComponent<Enemy1Movement>().movementStopped = false;
            GetComponent<Enemy1Attack>().isAttacking = false;
        }
        
    }
}
>>>>>>> Player
