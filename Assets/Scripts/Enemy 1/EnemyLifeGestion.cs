﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLifeGestion : MonoBehaviour
{
    public int life = 3;
    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("PlayerBullet"))
        {
            life--;
        }

        if (other.gameObject.CompareTag("PlayerAttack"))
        {
            life -= 3;
        }
        
        if (life <= 0)
        {
            Destroy(gameObject);
        }
    }
}
