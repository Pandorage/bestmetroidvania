﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Diagnostics;

public class PlayerAttack : MonoBehaviour
{
    public float attackRate;
    public float dashDistance = 10f;
    public LayerMask wallLayer;
    public PlayerMovement _mov;
    public GameObject baseAttack;
    
    private Animator _anim;
    private Collider2D _col;
    private float _nextAttack;
    private void Start()
    {
        _anim = GetComponent<Animator>();
        _mov = GetComponent<PlayerMovement>();
        _col = GetComponent<Collider2D>();
    }

    void Update()
    {
        if (Input.GetButtonDown("Attack") && Time.time >= _nextAttack && CanAttack() && _mov.canMove.value)
        {
            Attack();
            _nextAttack = Time.time + attackRate;
        }
        Debug.DrawRay(transform.position, _mov.lastDir * dashDistance,Color.red);
    }

    private void Attack()
    {
        _anim.SetTrigger("Attack");
        // Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(transform.position +(Vector3) (_mov.lastDir*attackRange),
        //     attackRadius, enemyLayer);
        // foreach (var enemy in hitEnemies)
        // {
        //     Destroy(enemy.gameObject);
        // }
        Transform attackTransform = Instantiate(baseAttack, (Vector2) transform.position + _mov.lastDir * (dashDistance / 2), Quaternion.identity).transform;
        Quaternion rotation = Quaternion.LookRotation
            (transform.position - attackTransform.position, attackTransform.TransformDirection(Vector3.back));
        attackTransform.rotation = new Quaternion(0, 0, rotation.z, rotation.w);
        transform.Translate(_mov.lastDir*dashDistance);
    }

    private bool CanAttack()
    {
        //return Physics2D.Raycast(transform.position, _mov.lastDir,dashDistance,wallLayer).collider == null;
        return Physics2D.BoxCast(transform.position,_col.bounds.size,0,_mov.lastDir, dashDistance,wallLayer).collider == null;
    }
}
