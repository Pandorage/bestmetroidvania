﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public FloatVariable movementSpeed;
    public BoolVariable canMove;
    private Rigidbody2D _rb;
    private Vector2 _movement;
    public Vector2 lastDir = new Vector2(0,-1);
    private Animator _anim;
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
        _anim.SetFloat("LastH", lastDir.x);
        _anim.SetFloat("LastV", lastDir.y);
        canMove.value = false;
    }
    
    void Update()
    {
        if (canMove.value)
        {
            _movement.x = Input.GetAxisRaw("Horizontal");
            _movement.y = Input.GetAxisRaw("Vertical");
            float coef = 1;
            if (_movement.x != 0 && _movement.y != 0)
            {
                coef = 0.75f;
            }
            if (!(_movement.x == 0 && _movement.y == 0))
            {
                lastDir = _movement* coef;
                _anim.SetFloat("LastH", lastDir.x);
                _anim.SetFloat("LastV", lastDir.y);
            }
            _anim.SetFloat("Horizontal", _movement.x);
            _anim.SetFloat("Vertical", _movement.y);
            _anim.SetFloat("Speed", _movement.sqrMagnitude);
            _rb.velocity = (_movement * movementSpeed.value * coef);
        }
    }

    public void CanMove()
    {
        canMove.value = true;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            Destroy(gameObject);
        }
    }
}
